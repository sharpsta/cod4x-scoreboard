### Server Status XML watcher script (copies the server's automatically generated
### XML file to a specified directory every few seconds)
### (Place this file in your CoD4x server directory)
###
### Replace "Path\To\Server" in $watcher.Path = "Path\To\Server" with the directory of your Cod4x server
### Replace "Path\To\cod4x_scoreboard" in Copy-Item "serverstatus.xml" -Destination "Path\To\cod4x_scoreboard"
###         with the path to your Cod4x_scoreboard directory
###
###
    $watcher = New-Object System.IO.FileSystemWatcher

### Path to Cod4x server
    $watcher.Path = "E:\COD4 Server\"
	$watcher.Filter = "serverstatus.xml"
	$watcher.IncludeSubdirectories = $false
	$watcher.EnableRaisingEvents = $true  

### Create an action that copies the generated XML file to the Cod4x Scoreboard directory
	$action = { Copy-Item "E:\COD4 Server\serverstatus.xml" -Destination "F:\xampp\www\cod4x_scoreboard\" } 

### Copy the status XML file when it is modified
    Register-ObjectEvent $watcher "Created" -Action $action
    Register-ObjectEvent $watcher "Changed" -Action $action	
	while ($true) {sleep 5}