<?php
header('Content-Type: application/json');

include('./scripts/config.php');
include('./scripts/functions.php');

//Load server status XML file
$server = null;

if (file_exists('serverstatus.xml')) {
  $server = simplexml_load_file('serverstatus.xml');
  if ($server === false) {
    $err['error'] = "Unable to read server status file.";
    echo json_encode($err);
    die;
  }
} else {
  $err['error'] = "Server status file not found.";
  echo json_encode($err);
  die;
}

$server_info = array();

try {
  //Connect to the database
  $db = new SQLite3('all_players.db');

  //Get the map name, server name, game type and map start time
  foreach($server->Game->Data as $data) {
    $value = strval($data['Value']);

    switch($data['Name']) {
      case 'mapname':
        $server_info['mapName'] = $value;
        break;

      case 'g_gametype':
        $server_info['gameType'] = $value;
        break;

      case 'sv_hostname':
        $server_info['hostName'] = $value;
        break;

      case 'g_mapStartTime':
        $server_info['mapStartTime'] = $value;

      default:
        break;
    }
  }

  //Get scores for each team
  $teamScores = array(intval($server->Game['TeamScoreA']), intval($server->Game['TeamScoreB']));

  //All players in-game
  $players = array();

  //Players in Spetsnaz or Opfor team
  $p_team0 = array();

  //Players in S.A.S. or Marines team
  $p_team1 = array();

  foreach($server->Clients->Client as $player) {
    $attrs = $player->attributes();

    //Player's IP address
    $cid = strval($attrs{'IP'});

    $players[$cid] = array(
      'name' => strval($attrs{'ColorName'}),
      'teamName' => strval($attrs{'TeamName'}),
      'score' => intval($attrs{'Score'}),
      'kills' => intval($attrs{'Kills'}),
      'deaths' => intval($attrs{'Deaths'}),
      'assists' => intval($attrs{'Assists'}),
      'totalKills' => 0,
      'totalScore' => 0
    );

    //Add the player into their respective team array (in free-for-all, players are added to the second table if the first table is full)
    if($players[$cid]['teamName'] == 'S.A.S.' || $players[$cid]['teamName'] == 'Marines' || ($players[$cid]['teamName'] == 'Free' && count($players) <= PLAYER_ROWS_PER_TEAM)) {
      array_push($p_team0, $players[$cid]);
    } else if($players[$cid]['teamName'] == 'Spetsnaz' || $players[$cid]['teamName'] == 'Opfor' || ($players[$cid]['teamName'] == 'Free' && count($players) > PLAYER_ROWS_PER_TEAM)) {
      array_push($p_team1, $players[$cid]);
    }
  }

  //Read in player details from the database (for keeping track of score/kills across the entire session)
  $players_db_entries = array();
  $player_query = $db->query("SELECT * FROM players;");
  while($row = $player_query->fetchArray()) {
    $cid = strval($row['id']);
    $kills_db = intval($row['kills']);

    $players_db_entries[$cid] = array(
      'id' => $cid,
      'name' => strval($row['name']),
      'teamName' => strval($row['team_name']),
      'score' => intval($row['score']),
      'kills' => $kills_db,
      'deaths' => intval($row['deaths']),
      'assists' => intval($row['assists']),
      'totalKills' => intval($row['total_kills']), //Total number of kills over all played maps
      'totalScore' => intval($row['total_score']) //Total score over all played maps
    );

    if(count($players) > 0 && isset($players[$cid])) {
      if($players[$cid]['kills'] < $players_db_entries[$cid]['kills']) {
        /*Set change in kills to zero if value from server status file is less than that in the DB
        (such as when a new map is started)*/
        $change = 0;
      } else {
        //Otherwise, set change to difference between values in server status file and DB
        $change = $players[$cid]['kills'] - $players_db_entries[$cid]['kills'];
      }

      $players_db_entries[$cid]['totalKills'] += $change;

      //Calculate total score for each player
      $change = 0;
      if($players[$cid]['score'] < $players_db_entries[$cid]['score']) {
        $change = 0;
      } else {
        $change = $players[$cid]['score'] - $players_db_entries[$cid]['score'];
      }

      $players_db_entries[$cid]['totalScore'] += $change;
      $players[$cid]['totalKills'] = $players_db_entries[$cid]['totalKills'];
      $players[$cid]['totalScore'] = $players_db_entries[$cid]['totalScore'];
    }
  }

  /*Get names of teams on the current map (unless the game type is
     free for all)*/
  if($server_info['gameType'] != 'dm') {
    $teams = get_team_names($server_info['mapName']);
  } else {
    $teams = array('Free', 'Free');
  }

  //Get name of team that is currently in the lead
  $team_in_lead = 'Tied';
  if ($server_info['gameType'] != 'dm') {
    if ($teamScores[0] > $teamScores[1]) {
      $team_in_lead = 'A';
    } else if ($teamScores[0] < $teamScores[1]) {
      $team_in_lead = 'B';
    }
  }

  $qry = $db->query("SELECT * FROM games;");
  $all_games = array();
  while($row = $qry->fetchArray(SQLITE3_NUM)) {
    array_push($all_games, $row);
  }


  //Get the name of the player who has the most kills across ALL games played plus their total number of kills
  $qry = $db->query("SELECT name, MAX(total_kills) FROM players;");
  $result = $qry->fetchArray(SQLITE3_NUM);
  $htk = array('', 0);
  if($result) {
    $htk = array($result[0], $result[1]);
  }

  //Get the name of the player who has the highest score across ALL games played plus their total score
  $qry = $db->query("SELECT name, MAX(total_score) FROM players;");
  $result = $qry->fetchArray(SQLITE3_NUM);
  $hts = array('', 0);
  if($result) {
    $hts = array($result[0], $result[1]);
  }

  //Get the number of games each team has won
  $qry = "SELECT lead, COUNT(*) FROM games WHERE lead != 'Tied' AND in_progress = 0 GROUP BY lead ORDER BY lead ASC;";
  $gamesWonA = 0; $gamesWonB = 0;

  $result = $db->query($qry);
  $row = null;
  while ($row = $result->fetchArray(SQLITE3_NUM)) {
    if ($row[0] == 'A') {
      $gamesWonA = $row[1];
    } else if ($row[0] == 'B') {
      $gamesWonB = $row[1];
    }
  }

  $gameInfo = array();
  $gameInfo['hostName'] = json_encode(string_colourise($server_info['hostName']));
  $gameInfo['map'] = get_map_full_name($server_info['mapName']);
  $gameInfo['gameType'] = get_gametype_full_name($server_info['gameType']);
  $gameInfo['highestTotalKills'] = $htk;
  $gameInfo['highestTotalScore'] = $hts;
  $teams = get_team_names($server_info['mapName']);
  $gameInfo['teamAName'] = $gameInfo['gameType'] != 'Free For All' ? $teams[0] : 'Free';
  $gameInfo['teamBName'] = $gameInfo['gameType'] != 'Free For All' ? $teams[1] : 'Free';
  $gameInfo['gamesWonA'] = $gamesWonA;
  $gameInfo['gamesWonB'] = $gamesWonB;
  $gameInfo['teamScoreA'] = $teamScores[0];
  $gameInfo['teamScoreB'] = $teamScores[1];

  if(count($all_games) == 0) {
	//If no previous games have been played on the server, add the current game into the games table
    $db->exec("INSERT INTO games(id, lead, in_progress, team_score_A, team_score_B) VALUES('{$server_info['mapStartTime']}', '{$team_in_lead}', 1, 0, 0);");
  }

  $game_starts = array();
  for ($i = 0; $i < count($all_games); $i++) {
    $game_starts[$i] = $all_games[$i][0];
  }

  if(!in_array($server_info['mapStartTime'], $game_starts, true)) {
    //Game has just started, so set the in-progress state of all other games to false and add the new game
    $db->exec("UPDATE games SET in_progress = 0;");
    $db->exec("INSERT INTO games(id, lead, in_progress, team_score_A, team_score_B) VALUES('{$server_info['mapStartTime']}', '{$team_in_lead}', 1, {$teamScores[0]}, {$teamScores[1]});");
  } else {
    //Update game that is currently in progress
    $db->exec("UPDATE games SET lead = '{$team_in_lead}', in_progress = 1, team_score_A = {$teamScores[0]}, team_score_B = {$teamScores[1]} WHERE id = '{$server_info['mapStartTime']}';");
  }

  foreach($players as $id => $p) {
    $stmt = null;
    if(isset($players_db_entries[$id])) {
	  //Update stats for existing players
      $stmt = "UPDATE players SET name = '{$p['name']}',
                             team_name = '{$p['teamName']}',
                                 score = {$p['score']},
                           total_score = {$p['totalScore']},
                                 kills = {$p['kills']},
                           total_kills = {$p['totalKills']},
                                deaths = {$p['deaths']},
                               assists = {$p['assists']}
                              WHERE id = '{$id}';";
    } else {
      //Add new players to database
      $stmt = "INSERT INTO players(id, name, team_name, score, total_score, kills, total_kills, deaths, assists)
               VALUES ('{$id}', '{$p['name']}', '{$p['teamName']}', {$p['score']}, {$p['totalScore']}, {$p['kills']}, {$p['totalKills']}, {$p['deaths']}, {$p['assists']});";
    }
    $db->exec($stmt);
  }

  //Build JSON object
  $obj = '{' . '"players": [';
  $first = true;
  foreach ($players as $p) {
    if ($first) {
      $first = false;
    } else {
      $obj .= ', ';
    };
    $obj .= '{ "name": "' . $p['name'] . '", "teamName": "' . $p['teamName'] . '", "kills": ' . $p['kills'] . ' , "totalKills": ' . $p['totalKills'];
    $obj .= ', "score": ' . $p['score'] . ', "totalScore": ' . $p['totalScore'] . ', "deaths": ' . $p['deaths'] . ', "assists": ' . $p['assists'] . ' }';
  }
  $obj .= '], "game": {';
  $obj .= '"hostName": ' . $gameInfo['hostName'] . ', "map": "' . $gameInfo['map'] . '", "gameType": "' . $gameInfo['gameType'] . '", "highestTotalKills": {';
  $obj .= '"name": "' . $gameInfo['highestTotalKills'][0] . '", "kills": ' . $gameInfo['highestTotalKills'][1] . '}, "highestTotalScore": {';
  $obj .= '"name": "' . $gameInfo['highestTotalScore'][0] . '", "kills": ' . $gameInfo['highestTotalScore'][1] . '}, ';
  $obj .= '"teamAName": "' . $gameInfo['teamAName'] . '", "teamBName": "' . $gameInfo['teamBName'] . '", ';
  $obj .= '"gamesWonA": ' . $gameInfo['gamesWonA'] . ', "gamesWonB": ' . $gameInfo['gamesWonB'] . ', ';
  $obj .= '"teamScoreA": ' . $gameInfo['teamScoreA'] . ', "teamScoreB": ' . $gameInfo['teamScoreB'] . ' ';
  $obj .= '}}';

  echo $obj;
} catch (Exception $e) {
  $err['error'] = $e->getMessage();
  echo json_encode($err);
} finally {
  //Close DB connection
  $db->close();
  unset($db);
}
?>
