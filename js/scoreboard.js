let container = document.getElementById('container');
let hostName = document.getElementById('host-name');
let gameTypeMapName = document.getElementById('gametype-mapname');
let divGameStats = document.getElementById('divGameStats');
let h3Team0Name = document.getElementById('h3Team0Name');
let h3Team1Name = document.getElementById('h3Team1Name');
let tblTeam0 = document.getElementById('tblTeam0');
let tblTeam1 = document.getElementById('tblTeam1');
let divMsg = document.getElementById('divMsg');
let divWFP = document.getElementById('divWFP');
let divError = document.getElementById('divError');
let h4HTKName = document.getElementById('htk-name');
let h4HTKKills = document.getElementById('htk-kills');
let h4HTSName = document.getElementById('hts-name');
let h4HTSScore = document.getElementById('hts-score');
let tdGamesWon0 = document.getElementById('games-won-0');
let tdGamesWon1 = document.getElementById('games-won-1');

const nRows = 12;
let playersTables = new Array(2);
let trPlayers = [new Array(12), new Array(12)];

buildPlayersTables();

setInterval(function() {
  getStats();
}, 5000);

async function getStats() {
  let rsp = await fetch('getStats.php');
  let obj = await rsp.json();
  if (obj['error']) {
    divMsg.style.display = 'flex';
    divWFP.style.display = 'none';
    divError.style.display = 'block';
    hostName.style.display = 'none';
    gameTypeMapName.style.display = 'none';
    divGameStats.style.display = 'none';
    divError.innerHTML = obj['error'];
  } else {
    hostName.innerHTML = obj['game']['hostName'];
    gameTypeMapName.innerHTML = obj['game']['gameType'] + ' | ' + obj['game']['map'];

    let teamImgs = "";
    if (obj['game']['gameType'] != 'Free For All') {
      teamImgs = [`<img src="./images/${obj['game']['teamAName']}_icon.webp" width="40" height="40" style="margin-right: 5px" />`, `<img src="./images/${obj['game']['teamBName']}_icon.webp" width="40" height="40" style="margin-right: 5px" />`];
      $(h3Team0Name).removeClass('free');
      $(h3Team1Name).removeClass('free');
      $('.team-table').find('th').removeClass('free');
    } else {
      teamImgs = ['', ''];
      $(h3Team0Name).addClass('free');
      $(h3Team1Name).addClass('free');
      $('.team-table').find('th').addClass('free');
    }
    h3Team0Name.innerHTML = teamImgs[0] + obj['game']['teamAName'];
    h3Team1Name.innerHTML = teamImgs[1] + obj['game']['teamBName'];

    let players = obj['players'];

    if (obj.players && obj.players.length > 0) {
      divMsg.style.display = 'none';
      hostName.style.display = 'block';
      gameTypeMapName.style.display = 'block';
      divGameStats.style.display = 'block';

      $('.player-row').children('td').empty();

      var playerCount = [0, 0];
      for (var i = 0; i < obj['players'].length; i++) {
        var player = obj['players'][i];
        let team = (player['teamName'] == 'Opfor' || player['teamName'] == 'Spetsnaz' || (player['teamName'] == 'Free' && i < nRows)) ? 0 : (player['teamName'] == 'Marines' || player['teamName'] == 'S.A.S.' || (player['teamName'] == 'Free' && i >= nRows)) ? 1 : -1;

        if (team > -1) {
          let tr;
          if (!trPlayers[team][playerCount[team]]) {
            tr = document.getElementById('team' + team + '_' + playerCount[team]);
            trPlayers[team][playerCount[team]] = tr;
          } else {
            tr = trPlayers[team][playerCount[team]];
          }
          tr.childNodes[0].innerHTML = player['name'];
          tr.childNodes[1].innerHTML = player['score'];
          tr.childNodes[2].innerHTML = player['kills'];
          tr.childNodes[3].innerHTML = player['deaths'];
          tr.childNodes[4].innerHTML = player['assists'];
		  
          playerCount[team]++;
        }
      }

      //Display the names of players with the highest total kills and score as well as the count for each
      h4HTKName.innerHTML = obj['game']['highestTotalKills']['name'];
      h4HTKKills.innerHTML = `(${obj['game']['highestTotalKills']['kills']})`;
      h4HTSName.innerHTML = obj['game']['highestTotalScore']['name'];
      h4HTSScore.innerHTML = `(${obj['game']['highestTotalScore']['kills']})`;

      //Display the number of games each team has won
      tdGamesWon0.innerText = obj['game']['gamesWonA'];
      tdGamesWon1.innerText = obj['game']['gamesWonB'];
    } else {
      //Display 'waiting for players' message
      divMsg.style.display = 'flex';
      divWFP.style.display = 'block';
      divError.style.display = 'none';
      hostName.style.display = 'none';
      gameTypeMapName.style.display = 'none';
      divGameStats.style.display = 'none';
    }
  } 
}

function buildPlayersTables() {
  let tbl;
  for (var k = 0; k < 2; k++) {
	tbl = tblTeam0;
    if (k == 1) { 
	  tbl = tblTeam1;
	}
    for (var i = 0; i < nRows; i++) {
	  //Add rows for each player slot
      tr = tbl.insertRow();
      tr.id = 'team' + k + '_' + i;
      tr.className = 'player-row';
      for (var j = 0; j < 5; j++) {
		//Add columns for each player's stats
        td = tr.insertCell(-1);
        if (j == 0) {
		  //Set overflow for player names
          td.style.overflow = "hidden";
          td.style.textOverflow = "ellipsis";
        } else {
          td.style.textAlign = "center";
        }
      }
    }
  }
  return tbl;
}
