<?php
// Number of rows to display in each team table
define('PLAYER_ROWS_PER_TEAM', 12);

// Default timezone
date_default_timezone_set('Australia/Brisbane');
