<?php
/**
 * Gets the names of both playing teams on a map
 * @param map Map filename to get teams for (e.g. mp_mapname)
 * @return Team names as an array
 */
function get_team_names($map)
{
  switch($map)
  {
    case 'mp_backlot':
    case 'mp_bog':
    case 'mp_broadcast':
    case 'mp_citystreets':
    case 'mp_convoy':
    case 'mp_crash':
    case 'mp_crash_snow':
    case 'mp_crossfire':
    case 'mp_showdown':
    case 'mp_strike':
      return array('Opfor', 'Marines');

    case 'mp_bloc':
    case 'mp_carentan':
    case 'mp_cargoship':
    case 'mp_countdown':
    case 'mp_creek':
    case 'mp_farm':
    case 'mp_killhouse':
    case 'mp_overgrown':
    case 'mp_pipeline':
    case 'mp_shipment':
    case 'mp_vacant':
      return array('Spetsnaz', 'S.A.S.');

    default:
      return array('Opfor', 'Marines');
  }
}

/**
 * Gets the name of a map as it appears in-game
 * @var string $map Map filename to get in-game name for, in the form mp_mapname
 * @return string Full name of map if found, else the map filename
 */
function get_map_full_name($map)
{
  switch($map)
  {
    case 'mp_convoy':
      return 'Ambush';

    case 'mp_backlot':
      return 'Backlot';

    case 'mp_bloc':
      return 'Bloc';

    case 'mp_bog':
      return 'Bog';

    case 'mp_countdown':
      return 'Countdown';

    case 'mp_crash':
      return 'Crash';

    case 'mp_crossfire':
      return 'Crossfire';

    case 'mp_citystreets':
      return 'District';

    case 'mp_farm':
      return 'Downpour';

    case 'mp_overgrown':
      return 'Overgrown';

    case 'mp_pipeline':
      return 'Pipeline';

    case 'mp_shipment':
      return 'Shipment';

    case 'mp_showdown':
      return 'Showdown';

    case 'mp_strike':
      return 'Strike';

    case 'mp_vacant':
      return 'Vacant';

    case 'mp_cargoship':
      return 'Wet Work';

    case 'mp_crash_snow':
      return 'Winter Crash';

    case 'mp_broadcast':
      return 'Broadcast';

    case 'mp_carentan':
      return 'Chinatown';

    case 'mp_creek':
      return 'Creek';

    case 'mp_killhouse':
      return 'Killhouse';

    default:
      return $map;
  }
}

/**
 * Returns the name of a game type as it appears in the game
 * @var string $type short-form name of game type to get in-game name of
 * @return string The full name of the game type (e.g. Search and Destroy for 'sd') if found, else the string that was passed in
 */
function get_gametype_full_name($type)
{
  switch($type)
  {
    case 'war':
      return 'Team Deathmatch';

    case 'dm':
      return 'Free For All';

    case 'dom':
      return 'Domination';

    case 'sab':
      return 'Sabotage';

    case 'sd':
      return 'Search and Destroy';

    case 'koth':
      return 'Headquarters';

    default:
      return $type;
  }
}

/**
 * Removes text colour tags (e.g. ^1, ^2, ...) from a colourised strings,
 * such as the server name
 * @var string $text text to remove colour tags from
 * @return string $text with colour tags removed
 */
function strip_colour_tags($text)
{
  return preg_replace('/\^\d/', '', $text);
}

/**
 * Replaces text colour tags (e.g. ^1, ^2, ...) with coloured text
 * (taken from https://github.com/volkv/CoD4X-Serverlist-Monitoring/blob/master/inc/functions.php)
 * @param string $text text to colourise
 * @return string Colourised string
 */
function string_colourise($text)
{
  $text .= '^';

  $colours = array(
    '/\^0(.*?)\^/is', // Grey
    '/\^1(.*?)\^/is', // Red
    '/\^2(.*?)\^/is', // Green
    '/\^3(.*?)\^/is', // Yellow
    '/\^4(.*?)\^/is', // Blue
    '/\^5(.*?)\^/is', // Aqua
    '/\^6(.*?)\^/is', // Magenta
    '/\^7(.*?)\^/is', // White
    '/\^8(.*?)\^/is', // Black
  );

  $tags = array(
    '<span style="color: #777;">$1</span>^',
    '<span style="color: #f44;">$1</span>^',
    '<span style="color: #0f0;">$1</span>^',
    '<span style="color: #ff0;">$1</span>^',
    '<span style="color: #3fbeff;">$1</span>^',
    '<span style="color: #0ff;">$1</span>^',
    '<span style="color: #f0f;">$1</span>^',
    '<span style="color: #fff;">$1</span>^',
    '<span style="color: #000;">$1</span>^',
  );

  $text = preg_replace($colours, $tags, $text);
  return substr($text, 0, strlen($text) - 1);
}

/**
 * Checks if a new map has been started, by checking if the current time
 * is within a certain range of a specified time
 * @param string $map_start_time starting time of current map
 * @return bool True if time difference is within 500ms, false if greater
 */
function game_is_new_map($map_start_time)
{
  $now = strtotime(date('D M y h:i:s Y'));
  $map_start_time = strtotime($map_start_time);
  $dd = $now - $map_start_time;

  if($dd < 500)
    return true;

  return false;
}
